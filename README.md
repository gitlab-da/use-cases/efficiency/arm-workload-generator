# ARM Workload Generator

This project implements CI/CD and code examples, using the [Hosted Runners on Linux ARM for GitLab.com](https://docs.gitlab.com/ee/ci/runners/hosted_runners/linux.html#machine-types-available-for-linux---arm64).

1. Rust project with calculating PI and allocating a long memory list - [src/main](src/main.rs), and a [full build, test, release, run CI/CD pipeline](.gitlab-ci.yml)
    - Code was generated using [GitLab Duo](https://about.gitlab.com/gitlab-duo/)
2. Storage artifact generator in CI/CD - [,gitlab-ci.yml](.gitlab-ci.yml)

## Pipeline schedule

Navigate to [`Build > Pipeline Schedules`](https://docs.gitlab.com/ee/ci/pipelines/schedules.html) and configure test values.

1. Every 15 minutes: Custom: `*/15 * * * *` 
1. Every day at 9am in the chosen timezone: `0 9 * * *`