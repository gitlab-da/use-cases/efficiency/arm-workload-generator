// Create main function
// Add code that takes a long time to compile
// Add missing dependencies


fn main() {
    let mut big_vec = Vec::with_capacity(100_000_000);
    for i in 0..100_000_000 {
        big_vec.push(i);
    }

    // Increase runtime

    let mut sum = 0;
    for i in &big_vec {
        sum += i;
    }
    println!("Sum of all elements: {}", sum);

    // Calculate Pi 
    use std::f64::consts::PI;

    let mut pi = 0.0;
    let mut k = 0.0;
    let mut sign = 1.0;

    for _i in 0..1_000_000 {
        pi += sign / (2.0 * k + 1.0);
        k += 1.0;
        sign *= -1.0;
    }

    pi *= 4.0;
    println!("Calculated value of Pi: {}", pi);
    println!("Actual value of Pi: {}", PI);

}
